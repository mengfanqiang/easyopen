package com.gitee.easyopen.util;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.log.NullLogChute;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

/**
 * Velocity工具类,根据模板内容生成文件
 * @author tanghc
 */
public class VelocityUtil {
    private static String UTF8 = "UTF-8";

    static {
        // 禁止输出日志
        Velocity.setProperty(Velocity.RUNTIME_LOG_LOGSYSTEM, new NullLogChute());
        Velocity.init();
    }

    private static String LOG_TAG = VelocityUtil.class.getName();

    public static void generate(VelocityContext context, InputStream inputStream, Writer writer) {
        Reader reader = new InputStreamReader(inputStream);

        Velocity.evaluate(context, writer, LOG_TAG, reader);

        try {
            writer.close();
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String generateToString(VelocityContext context, InputStream inputStream) {
        return generate(context, inputStream, UTF8);
    }

    public static String generate(VelocityContext context, InputStream inputStream, String charset) {
        return generateToString(context, new InputStreamReader(inputStream));
    }

    public static String generateToString(VelocityContext context, Reader reader) {
        StringWriter writer = new StringWriter();
        // 不用vm文件
        Velocity.evaluate(context, writer, LOG_TAG, reader);

        try {
            reader.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return writer.toString();

    }

}
